package org.kshrd.polymor;

public class Overloading {

    static int add(int a, int b) {
        return a + b;
    }

    static int add(int m, int n, int o) {
        return m + m + o;
    }

    static double add(double a, double b) {
        return a + b;
    }

    public static void main(String[] args) {
        System.out.println(Overloading.add(10,20));
        System.out.println(Overloading.add(28.38,38.2));
        System.out.println(Overloading.add(82,38,28));
    }


}
