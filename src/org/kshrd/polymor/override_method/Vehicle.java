package org.kshrd.polymor.override_method;

public class Vehicle {
    void makeNoise() {
        System.out.println("Vehicle noise");
    }
}

class Train extends Vehicle {

    void makeNoise() {
        System.out.println("Train noise");
    }
}

class Bike extends Vehicle {
    @Override
    void makeNoise() {
        System.out.println("Bike no noise");
    }
}

class MainClass {
    public static void main(String[] args) {
        Vehicle vehicle = new Vehicle();
        vehicle.makeNoise();

        Vehicle bike = new Bike();
        bike.makeNoise();
    }

}
