package org.kshrd.polymor;

public class Animal {

    void move() {
        System.out.println("Animals can move...");
    }

}

class Dog extends Animal {

    //@Override // Annotation
    void move() {
        System.out.println("Dog can run and walk...");
    }
}

class TestDog {
    public static void main(String[] args) {
        Animal a = new Animal();
        Animal b = new Dog();

        a.move();
        b.move();
    }
}


