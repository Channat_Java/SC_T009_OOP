package org.kshrd.encaptulation;

import java.text.DecimalFormat;

public class Score {

    private double math;
    private double physic;
    private double biology;
    private double average;

    public double getMath() {
        return math;
    }

    public void setMath(double math) {
        this.math = math;
    }

    public double getPhysic() {
        return physic;
    }

    public void setPhysic(double physic) {
        this.physic = physic;
    }

    public double getBiology() {
        return biology;
    }

    public void setBiology(double biology) {
        this.biology = biology;
    }

    public double getAverage() {
        return  Double.parseDouble(new DecimalFormat("#.##").format((math+physic+biology)/3));
    }
}
