package org.kshrd.intheritance;

public class Toyota extends Car {

    // Toyota has-a engine
    Engine engine = new Engine();

    void doSomething() {
        engine.changeOil();
    }
}

