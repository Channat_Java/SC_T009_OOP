package org.kshrd.intheritance;

public interface Walkable {
    void slowWalking();
}
