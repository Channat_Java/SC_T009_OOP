package org.kshrd.intheritance;

public interface Sleepable {
    void closeMyEyes();
}
