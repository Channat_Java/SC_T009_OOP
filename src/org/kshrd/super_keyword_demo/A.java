package org.kshrd.super_keyword_demo;

public class A {
    int aValue = 10;
    void myMethod() {
        System.out.println("my method in A class");
    }
    A() {
        System.out.println("A Constructor");
    }
}

class B extends A {
    int aValue = 20;
    void myMethod() {
        System.out.println("my method in B class");
    }
    B() {
        System.out.println("B Constructor");
        //super(); super() cannot use
        // under something else
    }

}

class C extends B {
    int aValue = 30;
    C() {
        // super() will automatic call even
        // we don't put it
        super();
    }

    void getValue() {
        System.out.println(aValue);
        super.myMethod();
    }

    public static void main(String[] args) {
        C c = new C();
        //c.getValue();
    }
}


