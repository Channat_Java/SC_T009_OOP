package org.kshrd.super_keyword_demo;

public class Car {
    String color = "White";
    void carMethod() {}
}

class Audi extends Car {
    String color = "Black";

    void printColor() {
        System.out.println(color); // black
        System.out.println(super.color); // white
        super.carMethod();
    }

    public static void main(String[] args) {
        Audi audi = new Audi();
        audi.printColor();
    }
}
